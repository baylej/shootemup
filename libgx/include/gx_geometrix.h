#ifndef _GX_GEO
#define _GX_GEO

#include "gx.h"

// Geometry functions

static inline GXpoint gx_Point(double x, double y)
{
	return (GXpoint){ .x = x, .y = y };
}

static inline GXvector gx_Vector(GXpoint pA, GXpoint pB)
{
	return (GXvector){ .x = pB.x - pA.x, .y = pB.y - pA.y };
}

static inline GXvector gx_Vector_XY(double x, double y)
{
	return (GXvector){ .x = x, .y = y };
}

static inline GXvector gx_Add_Vector(GXvector A, GXvector B)
{
	return (GXvector){ .x = A.x + B.x, .y = A.y + B.y };
}

static inline double gx_ProdScal(GXvector A, GXvector B)
{
	return A.x * B.x + A.y * B.y;
}

static inline double gx_Norme(GXvector p)
{
	return sqrt(p.x * p.x + p.y * p.y);
}

static inline double gx_ProdVect3P(GXpoint A, GXpoint B, GXpoint C)
{
	return (B.x - A.x) * (C.y - B.y) - (B.y - A.y) * (C.x - B.x);
} // To Check

static inline GXvector gx_ProdHMatVector(GXhmat m, GXvector v)
{
	return (GXvector){
		.x = m.m00 * v.x + m.m01 * v.y,
		.y = m.m10 * v.x + m.m11 * v.y };
} // To Check

static inline GXpoint gx_ProdHMatPoint(GXhmat m, GXpoint p)
{
	return (GXpoint){
		.x = m.m00 * p.x + m.m01 * p.y + m.m02,
		.y = m.m10 * p.x + m.m11 * p.y + m.m12 };
} // To Check

static inline GXhmat gx_MakeTranslationXY(double x, double y)
{
	return (GXhmat){
		.m00 = 0, .m01 = 0, .m02 = x,
		.m10 = 0, .m11 = 0, .m12 = y,
		.m20 = 0, .m21 = 0, .m22 = 1 };
} // To Check

static inline GXhmat gx_MakeHomothetieXY(double x, double y)
{
	return (GXhmat){
		.m00 = x, .m01 = 0, .m02 = 0,
		.m10 = 0, .m11 = y, .m12 = 0,
		.m20 = 0, .m21 = 0, .m22 = 1 };
} // To Check

static inline GXhmat gx_ProdHMat(GXhmat A, GXhmat B)
{
	return (GXhmat){
		.m00 = A.m00 * B.m00 + A.m01 * B.m10 + A.m02 * B.m20,    .m01 = A.m00 * B.m01 + A.m01 * B.m11 + A.m02 * B.m21,    .m02 = A.m00 * B.m02 + A.m01 * B.m12 + A.m02 * B.m22,
		.m10 = A.m10 * B.m00 + A.m11 * B.m10 + A.m12 * B.m20,    .m11 = A.m10 * B.m01 + A.m11 * B.m11 + A.m12 * B.m21,    .m12 = A.m10 * B.m02 + A.m11 * B.m12 + A.m12 * B.m22,
		.m20 = A.m20 * B.m00 + A.m21 * B.m10 + A.m22 * B.m20,    .m21 = A.m20 * B.m01 + A.m21 * B.m11 + A.m22 * B.m21,    .m22 = A.m20 * B.m02 + A.m21 * B.m12 + A.m22 * B.m22 };
} // To Check

static inline int gx_SegmentInterCercle(GXpoint A, GXpoint B, GXpoint C, double r, GXpoint *I, GXpoint *J)
{
	return 0;
} // TODO

#endif // _GX_GEO
