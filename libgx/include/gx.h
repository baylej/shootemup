#ifndef _GX_H
#define _GX_H

#pragma once

#include <stdlib.h>
#include <math.h>

// Init & Display

void gx_InitWindow(const char *winname, int w, int h);
void gx_SetWindowCoord(int xmin, int ymin, int xmax, int ymax);
void gx_SetDrawFunction(void (*)());
void gx_SetAnimationFunction(void (*)());
int gx_MainStart();

void gx_Show();
void gx_Clean();
int gx_Running();

int gx_GetXMin();
int gx_GetXMax();
int gx_GetYMin();
int gx_GetYMax();

// Geometry Types

struct _gx_2d {
	double x, y;
};
typedef struct _gx_2d GXpoint;
typedef struct _gx_2d GXvector;

struct _gx_mat {
	double m00, m01, m02;
	double m10, m11, m12;
	double m20, m21, m22;
};
typedef struct _gx_mat GXhmat; // TODO

// Math macros

#define PI        3.141592653589793
#define SQR(A)    ((A)*(A))
#define ZERO      1.e-6
#define IsZero(A) (A > -ZERO && A < ZERO)

// Input functions

GXpoint gx_GetMousePosition();
int     gx_GetClic();

// Colours

typedef unsigned char uchar;
typedef uchar GXcolour[3];

// Flashy colours
#define GXw (GXcolour){ 255, 255, 255 }

#define GXr (GXcolour){ 255,   0,   0 }
#define GXg (GXcolour){   0, 255,   0 }
#define GXb (GXcolour){   0,   0, 255 }

#define GXc (GXcolour){   0, 255, 255 }
#define GXm (GXcolour){ 255,   0, 255 }
#define GXy (GXcolour){ 255, 255,   0 }

// Shaded colours
#define GXwa (GXcolour){ 225, 225, 225 }
#define GXwb (GXcolour){ 195, 195, 195 }
#define GXwc (GXcolour){ 165, 165, 165 }
#define GXwd (GXcolour){ 135, 135, 135 }

#define GXba (GXcolour){   0,   0, 195 }
#define GXbb (GXcolour){   0,   0, 165 }

// Draw functions

void gx_Spotf(double x, double y, double r, GXcolour col);
void gx_Trianglef(double Ax, double Ay, double Bx, double By, double Cx, double Cy, GXcolour col, double thickness);
void gx_Circlef(double x, double y, double r, GXcolour col, double thickness);
void gx_Plotf_cs(double x, double y, GXcolour col, double thickness);
void gx_Linef(double Ax, double Ay, double Bx, double By, GXcolour col, double thickness);
void gx_BandWrite(const char *text, double x, double y, GXcolour col);
void gx_Grid(GXcolour a, GXcolour b);

#include "gx_geometrix.h"

#endif // _GX_H
