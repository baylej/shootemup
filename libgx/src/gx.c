#include <gx.h>

#include <stdio.h>

#include <GL/gl.h>
#include <GL/freeglut.h>

// GL proc addresses

static void (*glPointParameterf)(GLenum pname, GLfloat param) = NULL;

// Window coordinate system
// This code assumes that _xmin = -_xmax

static int win_w, win_h;
static int _xmin, _xmax, _ymin, _ymax;
static int _xsize, _ysize;

static inline double x_to_gl(double win_x)
{
	return win_x / _xmax;
}

static inline double y_to_gl(double win_y)
{
	return win_y / _ymax;
}

static inline double thick_to_gl(double thickness)
{
	return thickness;
}

// Screen to Window coordinates
static inline double mouse_to_x(double mouse_x)
{
	return ((mouse_x / (double)win_w) * 2 * _xmax) - _xmax;
}

static inline double mouse_to_y(double mouse_y)
{
	return (((win_h - mouse_y) / (double)win_h) * 2 * _ymax) - _ymax;
}

// Screen to GL coordinates
static inline double mousex_to_gl(double mouse_x)
{
	return mouse_x/(float)win_w*2.-1;
}

static inline double mousey_to_gl(double mouse_y)
{
	return (mouse_y/(float)win_h*2.-1)*-1.;
}

// Input management

static GXpoint mouse_pos;
static int mouse_clic;

static void mouse_move_listener(int x, int y)
{
	mouse_pos.x = mouse_to_x(x);
	mouse_pos.y = mouse_to_y(y);
}

static void mouse_clic_listener(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		mouse_clic = 1;
	} else {
		mouse_clic = 0;
	}
}

// Glut Callbacks

static void (*anim_f)(void) = NULL;
static void (*draw_f)(void) = NULL;

static void idle()
{
  if (anim_f) anim_f();
  glutPostRedisplay();
}

static void draw()
{
	if (!draw_f) return;
	draw_f();
	glutSwapBuffers();

	GLenum e = glGetError();
	if (e != GL_NO_ERROR) {
		fprintf(stderr, "OpenGL error %d occured, aborting\n", e);
		glutLeaveMainLoop();
	}
}

static void reshaped(int w, int h)
{
	win_w = w;
	win_h = h;
	glViewport(0, 0, w, h);
}

// API

void gx_InitWindow(const char *winname, int w, int h)
{
	int argc = 1; char *argv[] = {(char*)winname, NULL};

	_xsize = abs(_xmin) + _xmax;
	_ysize = abs(_ymin) + _ymax;

	glutInit(&argc, argv);

	glPointParameterf = (void (*)(GLenum, GLfloat))glutGetProcAddress("glPointParameterf");
	if (!glPointParameterf)
	{
		fprintf(stderr, "Could not get proc adress of glPointParameterf");
	}

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(w, h);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_MULTISAMPLE);
	glutCreateWindow(winname);
	glutReshapeFunc(reshaped);

	glutMouseFunc(mouse_clic_listener);
	glutPassiveMotionFunc(mouse_move_listener);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
}

void gx_SetDrawFunction(void (*draw_func)())
{
	draw_f = draw_func;
}

void gx_SetAnimationFunction(void (*anim_func)())
{
	anim_f = anim_func;
}

int gx_MainStart()
{
	glutDisplayFunc(draw);
	glutIdleFunc(idle);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutMainLoop();

	return 0;
}

void gx_Show()
{
	glutPostRedisplay();
}

void gx_Clean()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

int gx_Running()
{
	return 1; // TODO
}

void gx_SetWindowCoord(int xmin, int ymin, int xmax, int ymax)
{
	_xmin = xmin;
	_xmax = xmax;
	_ymin = ymin;
	_ymax = ymax;
}

int gx_GetXMin()
{
	return _xmin;
}

int gx_GetXMax()
{
	return _xmax;
}

int gx_GetYMin()
{
	return _ymin;
}

int gx_GetYMax()
{
	return _ymax;
}

GXpoint gx_GetMousePosition()
{
	return mouse_pos;
}

int gx_GetClic()
{
	int res = mouse_clic;
	mouse_clic = 0;
	return res;
}

void gx_Spotf(double x, double y, double r, GXcolour col)
{
	glPointSize(r * win_h / (2*_ymax));
	if (glPointParameterf) glPointParameterf(GL_POINT_FADE_THRESHOLD_SIZE, .5f);

	glBegin(GL_POINTS);
	{
		glColor3ubv(col);
		glVertex2d(x_to_gl(x), y_to_gl(y));
	}
	glEnd();

	if(glPointParameterf) glPointParameterf(GL_POINT_FADE_THRESHOLD_SIZE, 1.f);
}

void gx_Trianglef(double Ax, double Ay, double Bx, double By, double Cx, double Cy, GXcolour col, double thickness)
{
	glLineWidth(thick_to_gl(thickness));

	glBegin(GL_TRIANGLES);
	{
		glColor3ubv(col);
		glVertex2d(x_to_gl(Ax), y_to_gl(Ay));
		glVertex2d(x_to_gl(Bx), y_to_gl(By));
		glVertex2d(x_to_gl(Cx), y_to_gl(Cy));
	}
	glEnd();
}

void gx_Circlef(double x, double y, double r, GXcolour col, double thickness)
{
	glPointSize(r * win_h / (2*_ymax));

	glBegin(GL_POINTS);
	{
		glColor3ubv(col);
		glVertex2d(x_to_gl(x), y_to_gl(y));
	}
	glEnd();
}

void gx_Plotf_cs(double x, double y, GXcolour col, double thickness)
{
	glPointSize(thick_to_gl(thickness));

	glBegin(GL_POINTS);
	{
		glColor3ubv(col);
		glVertex2d(x_to_gl(x), y_to_gl(y));
	}
	glEnd();
}

void gx_Linef(double Ax, double Ay, double Bx, double By, GXcolour col, double thickness)
{
	glLineWidth(thick_to_gl(thickness));

	glBegin(GL_LINES);
	{
		glColor3ubv(col);
		glVertex2d(x_to_gl(Ax), y_to_gl(Ay));
		glVertex2d(x_to_gl(Bx), y_to_gl(By));
	}
	glEnd();
}

void gx_BandWrite(const char *text, double x, double y, GXcolour col)
{
	glColor3ubv(col);
	glRasterPos2f(mousex_to_gl(x), mousey_to_gl(y+5));
	glutBitmapString(GLUT_BITMAP_HELVETICA_10, (unsigned char*)text);
}

void gx_Grid(GXcolour a, GXcolour b)
{
	int x, y;

	glLineWidth(thick_to_gl(1.));

	glBegin(GL_LINES);
	{
		glColor3ubv(a);
		for (x = _xmin+1; x < _xmax; x++) {
			glVertex2d(x_to_gl(x), -1.);
			glVertex2d(x_to_gl(x),  1.);
		}
		glColor3ubv(b);
		for (y = _ymin+1; y < _ymax; y++) {
			glVertex2d(-1., y_to_gl(y));
			glVertex2d( 1., y_to_gl(y));
		}
	}
	glEnd();
}
