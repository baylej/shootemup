#include <math.h>
#include <gx.h>
#include <gx_geometrix.h>
#include "geometry.h"
#include "game.h"

/* Projection de pC sur [pA pB] */
double ProjPntDte(GXpoint pA, GXpoint pB, GXpoint pC, GXpoint *P) {
	GXvector ab, ac; double dist;
	ab = gx_Vector(pA, pB);
	ac = gx_Vector(pA, pC);
	dist = gx_ProdScal(ab, ac) / sqrnorm(ab);
        
	mulvector(ab, dist, ac);
	pointplusvector(pA, ac, *P);
	
	return dist;
}

/* Intersection entre [pA pB] et [pC pD], renvoit 1 si I est sur les 2 segments, sinon renvoit 0 */
int InterSegment(GXpoint pA, GXpoint pB, GXpoint pC, GXpoint pD, GXpoint *I) {
	GXvector cdOrth, ab, ac, cd, ci;
	GXpoint tmpI;
	double dist, cmp;
	
	normalvector2P(pC,pD,cdOrth);
	ab = gx_Vector(pA, pB);
	ac = gx_Vector(pA, pC);
	cd = gx_Vector(pC, pD);
	
	if (IsZero((dist = gx_ProdScal(ab, cdOrth)))) return 0;
	
	dist = gx_ProdScal(ac, cdOrth) / dist;
	mulvector(ab, dist, ac);
	pointplusvector(pA, ac, tmpI);
	
	ci = gx_Vector(pC, tmpI);
	
	if (I) *I = tmpI;
	return ((cmp = gx_ProdScal(ci, cd)) >= 0 && cmp <= sqrnorm(cd) && dist>= 0 && dist<=1) ? 1: 0; /* teste si I € [AB] */
}

/* si le triangle ABC est positif dans le sens trigo */
int isPositive(GXpoint pA, GXpoint pB, GXpoint pC) {
	return (gx_ProdVect3P(pA,pB,pC) >= 0) ? 1: 0;
}

/* Détermine si P appartient à ABC */
int isInTris(GXpoint pA, GXpoint pB, GXpoint pC, GXpoint P) {
	int cmp = isPositive(pA, pB, pC);
	
	if (cmp != isPositive(pA, pB, P)) return 0;
	if (cmp != isPositive(pB, pC, P)) return 0;
	if (cmp != isPositive(pC, pA, P)) return 0;
	
	return 1;
}

/* Renvoit le nombre d'intersections entre [AB] et l'ellipse de centre C, I = 1ère inter ; J = 2nd inter */
int SegmentInterEllipse(GXpoint pA, GXpoint pB, GXpoint pC, double rx, double ry, GXpoint *I, GXpoint *J) {
	GXpoint Ap, Bp, Ip, Jp;
	GXhmat direct, indirect;
	int n;
	
	direct = gx_MakeTranslationXY(pC.x, pC.y);
	direct = gx_ProdHMat(direct, gx_MakeHomothetieXY(rx, ry));
	
	indirect = gx_MakeHomothetieXY(1.0/rx, 1.0/ry);
	indirect = gx_ProdHMat(indirect, gx_MakeTranslationXY(-pC.x, -pC.y));
	
	Ap = gx_ProdHMatPoint(indirect, pA);
	Bp = gx_ProdHMatPoint(indirect, pB);
	
	n = gx_SegmentInterCercle(Ap, Bp, gx_Point(0.0,0.0), 1.0, &Ip, &Jp); /* Replace with InterSegmentCercle when done */

	*I = gx_ProdHMatPoint(direct, Ip);
	*J = gx_ProdHMatPoint(direct, Jp);
	
	return n;
}

/* Particule 'P' de vitesse 'v' va entrer en colision avec le cercle de centre C et de rayon r */
double interRayonCercle(GXpoint P, GXvector v, GXpoint C, double r, GXpoint *I) {
	GXvector pc, cp;
	double a, b, c, delta, dist;
	
	pc = gx_Vector(P, C);
	if (gx_ProdScal(v, pc)<0) return -1;
	
	cp = gx_Vector(C, P);
	a = sqrnorm(v);
	b = 2.0 * gx_ProdScal(v, cp);
	c = sqrnorm(cp) - SQR(r);
	delta = SQR(b) - 4.0*a*c;
	
	if (delta <= 0) return -1;
	
	dist = (-1.0*b-sqrt(delta)) / (2.0*a);
	
	mulvector(v, dist, v);
	pointplusvector(P, v, *I);
	
	return dist;
}

GXhmat makeRotMatrix(double alpha) { /* FIXME float et cosf() pour plus de perfs ? */
	GXhmat A;
	A.m00 = +cos(alpha); A.m01 = -sin(alpha); A.m02 = 0.0;
	A.m10 = +sin(alpha); A.m11 = +cos(alpha); A.m12 = 0.0;
	A.m20 = 0.0        ; A.m21 = 0.0        ; A.m22 = 1.0;
	return A;
}

#define SMALL 0.0055
#define IsSmall(x) ((x)<SMALL && (x)>-SMALL)

double Angle(GXvector A, GXvector B) { /* Assuming ||A|| = 1 */
	double res, tmp;
	
	res = gx_ProdScal(A, B);
	
	tmp = (res - sqrnorm(B));
	
	if (IsSmall(tmp)) return 0.0; /* moins que PI/30 */
	
	res = acosf(res/gx_Norme(B));
	return res * orientv(A, B);
}
