#include "output.h"
#include "pmake.h"
#include "geometry.h"

void make_isotris(GXpoint CoG, GXvector orient, double base_length, double height, GXpoint *A, GXpoint *B, GXpoint *C) {
	GXvector tmp, no;
	
	mulvector(orient, 2.0*height/3.0, tmp);
	pointplusvector(CoG, tmp, *A);
	
	mulvector(orient, -1.0*height/3.0, tmp);
	normalvector(orient, no);
	mulvector(no, base_length/2.0, no);
	tmp = gx_Add_Vector(tmp, no);
	pointplusvector(CoG, tmp, *B);
	
	mulvector(orient, -1.0*height/3.0, tmp);
	mulvector(no, -1.0, no);
	tmp = gx_Add_Vector(tmp, no);
	pointplusvector(CoG, tmp, *C);
}

void make_isogon(GXpoint C, double ray, int faces_count, GXpoint vertices[]) {
	GXvector iv;
	GXhmat rot_mat;
	float teta;
	int i;
	
	if (faces_count < 3 || ray < 0.2) return;
	
	iv = gx_Vector_XY(0.0, 1.0); /* vecteur de taille 1 */
	mulvector(iv, ray, iv);
	
	teta = 2.0*PI/(double)faces_count;
	rot_mat = makeRotMatrix(teta);
	
	pointplusvector(C, iv,vertices[0]);
	
	for (i=1; i<faces_count; i++) {
		iv = gx_ProdHMatVector(rot_mat, iv);
		pointplusvector(C, iv,vertices[i]);
	}
}
