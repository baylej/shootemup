#ifndef _GEO_H_
#define _GEO_H_
#include <gx.h>

/* geometry.h : Toutes les fonctions de calcul */

/* Quelques macros UTILISABLES avec -ansi -pedantic (pour les gurus) */
#define milieu                            /**/gx_Milieu
#define defpoint                          /**/gx_Point
#define defvectorxy                       /**/gx_Vector_XY
#define defvector                         /**/gx_Vector
#define addvector                         /**/gx_Add_Vector
#define prodscal                          /**/gx_ProdScal
#define prodvect                          /**/gx_ProdVect
#define prodvect3P                        /**/gx_ProdVect3P
#define orient                            /**/gx_Orient
#define dist                              /**/gx_Distance /* GOURMAND */
#define norm                              /**/gx_Norme /* GOURMAND */
/* Le dernier paramètre est OUT ! */
#define normalvector2P(A,B,V)             /**/(V).x=((A).y-(B).y); (V).y=((B).x-(A).x)
#define normalvector(V,N)                 /**/(N).x=(-(V).y); (N).y=((V).x)
#define difvector(U,V,D)                  /**/(D).x=((U).x-(V).x); (D).y((U).y-(V).y)
#define mulvector(U,a,V)                  /**/(V).x=((double)(a))*(U).x; (V).y=((double)(a))*(U).y
#define pointplusvector(A,V,B)            /**/(B).x=((A).x+(V).x); (B).y=((A).y+(V).y)
/* Left-Value macros */
#define sqrdist(A,B)                      /**/(SQR((B).x-(A).x)+SQR((B).y-(A).y))
#define sqrnorm(U)                        /**/(((U).x*(U).x)+((U).y*(U).y))
#define pntprodscal(A,B,C)                /**/((((B).x-(A).x)*((C).x-(A).x)) + (((B).y-(A).y)*((C).y-(A).y)))
#define orientv(U,V)                      /**/( ((U).x*(V).y>(U).y*(V).x)?+1:-1)


/* Fonctions de géo faites en TD */

/* Projection orthogonale de C sur [AB] */
double ProjPntDte(GXpoint A, GXpoint B, GXpoint C, GXpoint *P);

/* Intersection entre [AB] et [CD], renvoit 1 si I est sur les 2 segments, sinon renvoit 0 */
int InterSegment(GXpoint A, GXpoint B, GXpoint C, GXpoint D, GXpoint *I);

/* si le triangle ABC est positif dans le sens trigo */
int isPositive(GXpoint A, GXpoint B, GXpoint C);

/* Détermine si P appartient à ABC */
int isInTris(GXpoint A, GXpoint B, GXpoint C, GXpoint P);

/* Renvoit le nombre d'intersections entre [AB] et l'ellipse de centre C, I = 1ère inter ; J = 2nd inter */
int SegmentInterEllipse(GXpoint A, GXpoint B, GXpoint C, double rx, double ry, GXpoint *I, GXpoint *J);

/* renvoit -1 si pas d'intersection, sinon renvoit la distance entre P et I (I est l'intersection) */
double interRayonCercle(GXpoint P, GXvector v, GXpoint C, double r, GXpoint *I);

/* crée une matrice de rotation */
GXhmat makeRotMatrix(double alpha);

/* calcule l'angle(rad) de A,B ; renvoit 0 si < PI/30 ; norme(A) = 1 */
double Angle(GXvector A, GXvector B); /* SQRT !! (sauf < PI/30) */
#endif /* _GEO_H_ */
