#include "game.h"

/* fonctions d'affichage */

static void drawBullets() { /* gx_Spotf !! */ 
	struct _bullet *tmp;
	int i;
	iterator it;
	
	/* Custom FOREACH */
	for(it = iterator_init(&bullets), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
		tmp = (struct _bullet*) iterator_get(it);
		gx_Spotf(tmp->loc.x, tmp->loc.y, BU_RAY, GXb);
	}
}

static void drawStarShip() { /* Dessiner un triangle */
	gx_Trianglef(starship.A.x, starship.A.y, starship.B.x, starship.B.y, starship.C.x, starship.C.y, GXbb, LINE_THICK);
}

static void drawTurrets() { /* Dessiner un cercle */
	struct _turret * tmp;
	int i;
	iterator it;
	
	for(it = iterator_init(&turrets), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
		tmp = (struct _turret *) iterator_get(it);
		gx_Circlef(tmp->center.x, tmp->center.y, tmp->health*TU_RAY, tmp->color, LINE_THICK);
		gx_Plotf_cs(tmp->center.x, tmp->center.y, GXw, LINE_THICK);
	}
}

static void drawKamikazes() { /* Dessiner un polygone */
	struct _kamikaze * tmp;
	int i, k;
	iterator it;
	
	for(it = iterator_init(&kamikazes), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
		tmp = (struct _kamikaze *) iterator_get(it);
		
		if (tmp->v_length < 3) continue;
		
		for (k=0; k<tmp->v_length-1; k++) {
			gx_Linef(tmp->vertices[k].x, tmp->vertices[k].y, tmp->vertices[k+1].x, tmp->vertices[k+1].y, tmp->color, LINE_THICK);
		}
		gx_Linef(tmp->vertices[tmp->v_length-1].x, tmp->vertices[tmp->v_length-1].y, tmp->vertices[0].x, tmp->vertices[0].y, tmp->color, LINE_THICK);
	}
}

static void drawAsteroids() { /* Dessiner un polygone fixe */
	struct _asteroid * tmp;
	int i, k;
	iterator it;
	
	for(it = iterator_init(&asteroids), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
		tmp = (struct _asteroid *) iterator_get(it);
		
		if (tmp->v_length < 3) continue;
		
		for (k=0; k<tmp->v_length-1; k++) {
			gx_Linef(tmp->vertices[k].x, tmp->vertices[k].y, tmp->vertices[k+1].x, tmp->vertices[k+1].y, GXr, LINE_THICK);
		}
		gx_Linef(tmp->vertices[tmp->v_length-1].x, tmp->vertices[tmp->v_length-1].y, tmp->vertices[0].x, tmp->vertices[0].y, GXr, LINE_THICK);
	}
}

static void drawBoss() { /* Dessiner une suite de polygones */ }

static void drawUI() { /* affichage la barre de vie, de bouclier, de missile */
	char msg[64];
	
	sprintf(msg, "Level %d", gamemode); /* FIXME affichage sous forme de barre */
	gx_BandWrite(msg, 5,  5, GXw);
	
	sprintf(msg, "Health %5.2f", starship.health);
	gx_BandWrite(msg, 5, 20, GXw);
	
	//sprintf(msg, "Shield %5.2f", starship.shield); /* Not Yet Implemented */
	//gx_BandWrite(msg, 5, 35, GXw);
	
	//sprintf(msg, "Missiles %d", starship.missile); /* Not Yet Implemented */
	//gx_BandWrite(msg, 5, 50, GXw);
}

void draw() { /* Fonction de dessin principale */
	if (!gx_Running()) return;
	
	gx_Clean();
	gx_Grid(GXwb,GXwd);
	
	drawAsteroids();
	drawStarShip();
	drawBullets();
	
	if (gamemode == GAME_BOSS) {
		drawBoss();
	} else {
		drawTurrets();
		drawKamikazes();
	}
	drawUI();
}
