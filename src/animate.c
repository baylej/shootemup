#include <gx.h>
#include "game.h"
#include "geometry.h"

/* fonctions d'animation */

static int is_out_screen(GXpoint X) {
	return (X.x < gx_GetXMin() || X.x > gx_GetXMax() || X.y < gx_GetYMin() || X.y > gx_GetYMax());
}

/* renvoit 1 si bullet est en intersection avec [AB], 0 sinon */
static int bounce_bullet(struct _bullet * bullet, GXpoint A, GXpoint B) {
	GXvector nab;
	GXpoint C;
	GXhmat rot;
	double teta;
	
	if (!bullet) return 0;
	
	pointplusvector(bullet->loc, bullet->speed, C);
	if (!InterSegment(bullet->loc, C, A, B, NULL)) return 0;
	
	normalvector2P(A, B, nab);
	teta = Angle(bullet->speed, nab);
	
	rot = makeRotMatrix(2*teta);
	bullet->speed = gx_ProdHMatVector(rot, bullet->speed);
	bullet->speed.x = -bullet->speed.x;
	bullet->speed.y = -bullet->speed.y;
	return 1;
}

static void updateBullets() { /* fait avancer les balles, rebondir et détruire les ennemis */ 
	GXvector v;
	struct _bullet *tmp;
	void *obj;
	int i, j, k;
	iterator it, obs;
	
	for(it = iterator_init(&bullets), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) { /* FIXME intersections */
		tmp = (struct _bullet *) iterator_get(it);
		mulvector(tmp->speed, BSPEED_FACTOR, v);
		pointplusvector(tmp->loc, v, tmp->loc);
		if (is_out_screen(tmp->loc)) {
			free(iterator_remove(&it));
			continue;
		}
		
		/* Collision avec le vaisseau */
		if (isInTris(starship.A, starship.B, starship.C, tmp->loc)) {
			starship.health -= E_DAMAGE;
			free(iterator_remove(&it));
			continue;
		}
		
		/* Collision avec un asteroid */
		for(obs = iterator_init(&asteroids), j=(*(obs.list)==NULL)?0:1; j; j=iterator_next(&obs)) {
			obj = iterator_get(obs); /* (struct _asteroid *) */
			
			v = gx_Vector(tmp->loc, ((struct _asteroid *)obj)->loc);
			if (gx_ProdScal(v, tmp->speed)<0) continue;
			
			if (sqrdist(tmp->loc, ((struct _asteroid *)obj)->loc) <= SQR(AS_RAY + BU_RAY)) {
				for (k=1 ; k<((struct _asteroid *)obj)->v_length; k++) {
					if (bounce_bullet(tmp, ((struct _asteroid *)obj)->vertices[k-1], ((struct _asteroid *)obj)->vertices[k]))
						break;
				}
				bounce_bullet(tmp, ((struct _asteroid *)obj)->vertices[0], ((struct _asteroid *)obj)->vertices[k-1]);
			}
		}
		
		/* Collision avec une tourelle */
		for(obs = iterator_init(&turrets), j=(*(obs.list)==NULL)?0:1; j; j=iterator_next(&obs)) {
			obj = iterator_get(obs); /* (struct _turret *) */
			
			v = gx_Vector(tmp->loc, ((struct _turret *)obj)->center);
			if (gx_ProdScal(v, tmp->speed)<0) continue;
			
			if (sqrdist(tmp->loc, ((struct _turret *)obj)->center) <= ((struct _turret *)obj)->health * SQR(TU_RAY + BU_RAY)) {
				free(iterator_remove(&it)); tmp = NULL;
				((struct _turret *)obj)->health -= S_DAMAGE;
				if (((struct _turret *)obj)->health < 0.3) {
					free(iterator_remove(&obs));
				}
				break;
			}
		}
		if (!tmp) continue; /* si la balle a touchée une turret */
		
		/* Collision avec un kamikaze */
		for(obs = iterator_init(&kamikazes), j=(*(obs.list)==NULL)?0:1; j; j=iterator_next(&obs)) {
			obj = iterator_get(obs); /* (struct _kamikaze *) */
			
			v = gx_Vector(tmp->loc, ((struct _kamikaze *)obj)->loc);
			if (gx_ProdScal(v, tmp->speed)<0) continue;
			
			if (sqrdist(tmp->loc, ((struct _kamikaze *)obj)->loc) <= SQR(KA_RAY + BU_RAY)) {
				free(iterator_remove(&it)); tmp = NULL;
				((struct _kamikaze *)obj)->v_length--;
				if (((struct _kamikaze *)obj)->v_length < 3) 
					free(iterator_remove(&obs));
				else {
					make_isogon(((struct _kamikaze *)obj)->loc, KA_RAY, ((struct _kamikaze *)obj)->v_length, ((struct _kamikaze *)obj)->vertices);
				}
				break;
			}
		}
	}
}

static void updateStarShip() { /* fait suivre la souris et tirer si un clic est détecté */
	struct _bullet *tmp;
	
	GXpoint mp;
	GXvector AB, move;
	GXhmat rot;
	double angle;	/* FIXME : ne pas pénétrer les astéroides */
	
	mp = gx_GetMousePosition();
	AB = gx_Vector(starship.loc, mp);
	angle = Angle(starship.speed, AB);
	angle = angle/SMOOTH_ROT_FACTOR;
	rot = makeRotMatrix(angle);
	
	starship.speed = gx_ProdHMatVector(rot, starship.speed);
	
	make_isotris(starship.loc, starship.speed, STARSHIP_B, STARSHIP_H, &starship.A, &starship.B, &starship.C);
	
	if (SQR(STARSHIP_H) < sqrdist(starship.loc, mp)) {
	
		mulvector(starship.speed, SPEED_FACTOR, move);
		pointplusvector(starship.loc, move, starship.loc);
		pointplusvector(starship.A, move, starship.A);
		pointplusvector(starship.B, move, starship.B);
		pointplusvector(starship.C, move, starship.C);
	
	}
	
	if (gx_GetClic()) { /* shoot ! */
		tmp = (struct _bullet*) malloc(sizeof(struct _bullet));
		
		if (tmp) {
			tmp->loc = starship.A;
			tmp->speed = starship.speed;
			
			put_list_top(&bullets, tmp);
		}
	}
}

static void updateTurrets() { /* fait tirer les turrets */ 
	static iterator it = {.list = NULL};
	static int i = 0;
	struct _turret *tmp;
	struct _bullet *new;
	GXvector v;
	double norme;
	
	if ((++i) >= 20) {
		i=0;
	
		if((tmp = (struct _turret*)iterator_get(it))) {

			v = gx_Vector(tmp->center, starship.loc); /* FIXME tirer 'devant' le vaisseau */
			norme = gx_Norme(v);
			mulvector(v,1.0/norme,v);

			new = (struct _bullet*) malloc(sizeof(struct _bullet));
			if (new) {
				new->loc = tmp->center;
				new->speed = v;

				put_list_top(&bullets, new);
			}
		}

		if (iterator_has_next(it)) {
			iterator_next(&it);
		} else {
			it = iterator_init(&turrets);
		}
	}
}

static void updateKamikazes() { /* fait suivre le vaisseau */
	iterator it, obs;
	struct _kamikaze *tmp;
	void *obj;
	
	GXvector v, AB;
	GXhmat rot;
	double angle;
	
	int i, j, k;
	
	for(it = iterator_init(&kamikazes), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
		tmp = iterator_get(it);
		
		if (is_out_screen(tmp->loc)) {
			free(iterator_remove(&it));
			continue;
		}
		
		AB = gx_Vector(tmp->magic.loc, starship.loc);
		angle = Angle(tmp->magic.speed, AB);
		angle = angle/SMOOTH_ROT_FACTOR;
		rot = makeRotMatrix(angle);
		tmp->magic.speed = gx_ProdHMatVector(rot, tmp->magic.speed);
		
		mulvector(tmp->magic.speed, KSPEED_FACTOR, v);
		pointplusvector(tmp->magic.loc, v, tmp->magic.loc);
		
		AB = gx_Vector(tmp->loc, tmp->magic.loc);
		angle = Angle(tmp->speed, AB);
		angle = angle/SMOOTH_ROT_FACTOR;
		rot = makeRotMatrix(angle);
		tmp->speed = gx_ProdHMatVector(rot, tmp->speed);
		
		mulvector(tmp->speed, KSPEED_FACTOR, v);
		pointplusvector(tmp->loc, v, tmp->loc);
		
		for (j=0; j<tmp->v_length ; j++) {
			pointplusvector(tmp->vertices[j], v, tmp->vertices[j]);
		}

		/* Collision avec le vaisseau */
		if (isInTris(starship.A, starship.B, starship.C, tmp->loc)) {
			starship.health -= K_DAMAGE;
			free(iterator_remove(&it));
			continue;
		}
		
		/* Collision avec un asteroid */
		for(obs = iterator_init(&asteroids), j=(*(obs.list)==NULL)?0:1; j; j=iterator_next(&obs)) {
			obj = iterator_get(obs); /* (struct _asteroid *) */
			
			v = gx_Vector(tmp->magic.loc, ((struct _asteroid *)obj)->loc);
			if (gx_ProdScal(v, tmp->magic.speed)<0) continue;
			
			if (sqrdist(tmp->magic.loc, ((struct _asteroid *)obj)->loc) <= SQR(AS_RAY + KA_RAY)) {
				for (k=1 ; k<((struct _asteroid *)obj)->v_length; k++) {
					if (bounce_bullet(&(tmp->magic), ((struct _asteroid *)obj)->vertices[k-1], ((struct _asteroid *)obj)->vertices[k]))
						break;
				}
				bounce_bullet(&(tmp->magic), ((struct _asteroid *)obj)->vertices[0], ((struct _asteroid *)obj)->vertices[k-1]);
			}
		}
	}
}

static void updateBoss() { /* fait avancer le boss */
	
}

void animate(void) {
	updateBullets();
	updateStarShip();
	
	if (gamemode == GAME_BOSS) {
		updateBoss();
	} else {
		updateKamikazes();
		updateTurrets();
		
		if (get_list_len(kamikazes) == 0 && get_list_len(turrets) == 0)
			gamemode = GAME_BOSS;
	}
	
	gx_Show(); /* !! CETTE FONCTION NE RETOURNE PAS IMMEDIATEMENT !! */
}
