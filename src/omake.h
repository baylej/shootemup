#ifndef _OMAKE_H_
#define _OMAKE_H_

/* omake.h : génération des astéroides, ennemis, du BOSS */
void makeAsteroids();

void makeTurrets();

void makeKamikazes();

void makeBoss();

#endif	/* _OMAKE_H_ */

