/* Fichier Main du projet */
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "game.h"

int gamemode = GAME_NEW; /* le niveau (1, 2, 3, 4, 5, ...) ; BOSS est le niveau 100, 0: le jeu n'a pas commencé */

struct _starship starship; /* Starship contient les valeurs du vaisseau contrôlé par le joueur */
list asteroids = NULL; /* Astéroides la valeur est un struct _asteroid */
list turrets = NULL; /* Turrets */
list kamikazes = NULL; /* Kamikaze */
list bullets = NULL; /* Bullets */

void initGlobs() { /* à appeler à chaque niveau */
	srand(time(NULL));
	
	/* TODO Initialisation des variables globales ICI */
	gamemode++;
	
	starship.loc = INITIAL_S_POS; /* Spawn Point */
	starship.health = INITIAL_S_HP;
	//starship.shield = 0.0;
	//starship.missile = 0;
	starship.speed = INITIAL_SPEED;
	make_isotris(starship.loc, starship.speed, STARSHIP_B, STARSHIP_H, &(starship.A), &(starship.B), &(starship.C));
	
	makeAsteroids();
	
	if (gamemode == GAME_BOSS) {
		makeBoss();
	} else {
		makeTurrets();
		makeKamikazes();
	}
}

/* Initialisation de la GX */
int init(void) {
	gx_InitWindow(W_TITLE, W_WIDTH, W_HEIGHT);
	gx_SetWindowCoord(-15, -10, 15, 10); /* DEBUG */
	
	initGlobs();
	
	gx_SetDrawFunction(draw);
	gx_SetAnimationFunction(animate);
	return gx_MainStart();
}

void cleanup() {
	iterator it;
	void * get;
	
	it = iterator_init(&asteroids);
	while (get = iterator_get(it), get) {
		free((struct _asteroid *)get);
	}
	free_str_list_base_data(&asteroids);
	
	it = iterator_init(&kamikazes);
	while (get = iterator_get(it), get) {
		free((struct _kamikaze *)get);
	}
	free_str_list_base_data(&kamikazes);
	
	free_str_list_base_data(&turrets);
	free_str_list_base_data(&bullets);
}

int main(int argc, char *argv[]) {
	return init();
}
