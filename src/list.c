#include "list.h"

/* ajout en tete de liste */
int put_list_top(struct _str_list_base **head, void *data) {
	struct _str_list_base *newe;
	int res = 0;
	
	newe = (struct _str_list_base *)malloc(sizeof(struct _str_list_base));
	if(newe != NULL) {
		newe->next = *head;
		*head = newe;
		newe->data = data;
		res = 1;
	}
	
	return res;
}

/* prend la tete de liste */
void * rem_list_top(struct _str_list_base **head) {
	void *res = NULL;
	struct _str_list_base *tmp;
	if (*head == NULL) return NULL;
	res = (*head)->data;
	tmp = *head;
	*head = (*head)->next;
	free(tmp);
	return res;
}

/* ajout en queue de liste */
int put_list_bottom(struct _str_list_base **head, void *data) {
	struct _str_list_base *newe;
	int res=0;
	if (*head != NULL) {
		while ((*head)->next != NULL) {
			head = &((*head)->next);
		}
		newe = (struct _str_list_base *)malloc(sizeof(struct _str_list_base));
		if(newe != NULL) {
			newe->next = NULL;
			newe->data = data;
			(*head)->next = newe;
			res = 1;
		}
	} else {
		newe = (struct _str_list_base *)malloc(sizeof(struct _str_list_base));
		if(newe != NULL) {
			newe->next = NULL;
			newe->data = data;
			*head = newe;
			res = 1;
		}
	}
	return res;
}

/* Renvoit le penultieme element de la liste */
static struct _str_list_base * getPenultimateElement(struct _str_list_base *str) {
	if (str != NULL) {
		if (str->next != NULL) {
			while ((str->next)->next != NULL) {
				str = str->next;
			}
		}
	}
	return str;
}

void * rem_list_bottom(struct _str_list_base **head) {
	void *data = NULL;
	struct _str_list_base *tmp;
	tmp = getPenultimateElement(*head);
	
	if (tmp != NULL) {
		if (tmp == *head && tmp->next == NULL) {
			data = tmp->data;
			free(tmp);
			*head = NULL;
		} else {
			data = (tmp->next)->data;
			free(tmp->next);
			tmp->next = NULL;
		}
	}
	return data;
}

/* longueur de la liste */
int get_list_len(struct _str_list_base *head) {
	int i=0;
	
	while (head != NULL) {
		i++;
		head = head->next;
	}
	
	return i;
}

/* libere la liste mais pas les donnees quelle contient */
void free_str_list_base(struct _str_list_base **ptr) {
	struct _str_list_base *next;
	
	if(*ptr == NULL) { return; }
	next = (*ptr)->next;

	while (1) {
		free(*ptr);
		*ptr = next;
		if (next->next == NULL) {
			break;
		} else {
			next = next->next;
		}
	}
	/* On a besoin de l'adresse du pointeur pour le mettre a NULL */
	*ptr = NULL;
}

/* libere la liste et les donnees quelle contient */
void free_str_list_base_data(struct _str_list_base **ptr) {
	struct _str_list_base *next;
	
	if(*ptr == NULL) { return; }
	next = (*ptr)->next;

	while (1) {
		free((*ptr)->data);
		free(*ptr);
		*ptr = next;
		if (next->next == NULL) {
			break;
		} else {
			next = next->next;
		}
	}
	/* On a besoin de l'adresse du pointeur pour le mettre a NULL */
	*ptr = NULL;
}

/* initialise l'iterator */
iterator iterator_init(struct _str_list_base **head) {
	iterator it; it.list=head;
	return it;
}

/* tout aussi easy que la fonction precedente */
int iterator_has_next(iterator it) {
	if (it.list && *(it.list))
		return ((*(it.list))->next) != NULL;
	return 0;
}

/* Passe au chainon suivant, renvoit 0 si NULL, 1 sinon */
int iterator_next(iterator *it) {
	if (it && it->list && *(it->list)) {
		it->list = &((*(it->list))->next);
	}
	return *(it->list) != NULL;
}

/* retourne le contenu du chainon actuel */
void * iterator_get(iterator it) {
	if (it.list && *(it.list))
		return (*(it.list))->data;
	return NULL;
}

/* retourne le contenu du chainon actuel et supprime le chainon */
void * iterator_remove(iterator *it) {
	void *res = NULL;
	struct _str_list_base *tmp;
	
	if (it && it->list && *(it->list)) {
		tmp = *(it->list);
		res = tmp->data;
		/* Cas tete de liste */
		*(it->list) = (*(it->list))->next;
		free(tmp);
		return res;
	}
		
	return NULL;
}
