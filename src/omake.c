#include <stdlib.h>
#include <string.h>
#include "output.h"
#include "list.h"
#include "game.h"
#include "geometry.h"
#include <gx.h>

/* 'x' ou 'y' */
static double randCoord(char coord) { /* FIXME rajouter un offset pour éviter les spawn proche des bords et du centre */
	int div, gap;
	double res;
	
	if (coord == 'x') {
		gap = abs(gx_GetXMin());
		div = gap + gx_GetXMax(); /* Assuming Xmax and Ymax are > 0 */
	} else {
		gap = abs(gx_GetYMin());
		div = gap + gx_GetYMax();
	}
	
	div *=100;
	div +=1;
	
	res = (double) (rand() % div) / 100.0;
	res -= (double)gap;
	
	return res;
}

#define C_AS 1
#define C_TU 2
#define C_KA 4
#define C_SS 8

/* Vérifie la collision avec un autre objet, tocheck est un boolean des flags ci-dessus, la valeur renvoyée aussi */
static int checkCoords(double x, double y, double dist, int tocheck) {
	GXpoint T;
	iterator it;
	void *ptr;
	int i, res = 0;
	
	T = gx_Point(x, y);
	
	if (tocheck & C_SS) {
		if (sqrdist(T, starship.loc) < SQR(dist+STARSHIP_H))
			res |= C_SS;
	}
	
	if (tocheck & C_AS) {
		for(it = iterator_init(&asteroids), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
			ptr = iterator_get(it);
			if (sqrdist(T, ((struct _asteroid*)ptr)->loc) < SQR(dist+AS_RAY)) {
				res |= C_AS;
				break;
			}
		}
	}
	
	if (tocheck & C_TU) {
		for(it = iterator_init(&turrets), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
			ptr = iterator_get(it);
			if (sqrdist(T, ((struct _turret*)ptr)->center) < SQR(dist+TU_RAY)) { /* assuming all turrets have 100hp */
				res |= C_TU;
				break;
			}
		}
	}
	
	if (tocheck & C_KA) {
		for(it = iterator_init(&kamikazes), i=(*(it.list)==NULL)?0:1; i; i=iterator_next(&it)) {
			ptr = iterator_get(it);
			if (sqrdist(T, ((struct _kamikaze*)ptr)->loc) < SQR(dist+KA_RAY)) {
				res |= C_KA;
				break;
			}
		}
	}
	return res;
}

void makeAsteroids() {
	struct _asteroid *new;
	int i, count;
	GXpoint C;
	
	count = gamemode * ENEMY_FACTOR; /* FIXME */
	
	for (i=0; i<count; i++) {
		do {
			C.x = randCoord('x');
			C.y = randCoord('y');
		} while (checkCoords(C.x, C.y, AS_RAY, C_SS | C_AS));
		
		new = (struct _asteroid *) malloc(sizeof(struct _asteroid));
		if (!new) ABORT(, "L'allocation d'un astéroide a échouée !")
		
		new->v_length = (rand()%(MAX_VGONE-3))+4; /* Au moins 3 cotés */
		new->loc = C;
		
		new->vertices = (GXpoint *) malloc(sizeof(GXpoint) * new->v_length);
		if (!new) ABORTF(, "L'allocation des cotés d'un astéroide a échouée !", new)

		make_isogon(C, AS_RAY, new->v_length, new->vertices);
		put_list_top(&asteroids, (void*)new);
	}
}

void makeTurrets() {
	struct _turret *new;
	int i, count;
	
	count = gamemode * ENEMY_FACTOR;
	
	for (i=0; i<count; i++) {
		new = (struct _turret *) malloc(sizeof(struct _turret));
		if (!new) ABORT(, "L'allocation d'une tourelle a échouée !")
		
		new->health = 1.0;
		do {
			new->center.x = randCoord('x');
			new->center.y = randCoord('y');
		} while (checkCoords(new->center.x, new->center.y, AS_RAY, C_SS | C_AS | C_TU));
		memcpy(new->color, GXg, 3);
		
		put_list_top(&turrets, (void*)new);
	}
}

void makeKamikazes() { /* FIXME les générer en dehors de la map */
	struct _kamikaze *new;
	int i, count;
	GXpoint C;
	
	count = gamemode * ENEMY_FACTOR;
	
	for (i=0; i<count; i++) {
		do {
			C.x = randCoord('x'); /* FIXME collisions */
			C.y = randCoord('y');
		} while (checkCoords(C.x, C.y, AS_RAY, C_SS | C_AS | C_TU | C_KA));
		
		new = (struct _kamikaze *) malloc(sizeof(struct _kamikaze));
		if (!new) ABORT(, "L'allocation d'un kamikaze a échouée !")
		
		new->v_length = MAX_VGONE;
		new->speed = INITIAL_SPEED; /* FIXME rendre l'orientation aléatoire (matrice rotation) */
		new->loc = C;
		memcpy(new->color, GXc, 3);
		new->magic.loc = C;
		new->magic.loc.y++;
		new->magic.speed = INITIAL_SPEED;
		
		new->vertices = (GXpoint *) malloc(sizeof(GXpoint) * new->v_length);
		if (!new) ABORTF(, "L'allocation des cotés d'un kamikazea échouée !", new)

		make_isogon(C, KA_RAY, new->v_length, new->vertices);
		put_list_top(&kamikazes, (void*)new);
	}
}

void makeBoss() {
	
}
