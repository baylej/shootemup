/**
 * @file list.h
 * @author Jonathan Bayle (hide@mrhide.fr)
 * @brief création et gestion de listes, piles, queue, deque
 * @date 03/2012
 * @remarks Code portable
 */

#ifndef _COLLECTIONS_H_
#define _COLLECTIONS_H_

#include <stdlib.h>

/**
 * @brief Structure de base des collections List Stack Queue et Deque
 */
struct _str_list_base {
	void *data;
	struct _str_list_base *next;
};

/* Liste chainée */

/**
 * @brief un élément de la liste
 * @remarks Ne pas oublier de me déclarer à NULL : <code>list *head = NULL;</code>
 */
typedef struct _str_list_base * list;

/**
 * Ajoute une donnée en tête de liste
 * @param[in] head L'adresse de la tête de la liste
 * @param[in] data L'adresse de la donnée
 * @return 1 si succès, 0 si erreur
 */
int put_list_top(struct _str_list_base **head, void *data);

/**
 * Ajoute une donnée en fin de liste
 * @param[in] head L'adresse de la tête de la liste
 * @param[in] data L'adresse de la donnée
 * @return 1 si succès, 0 si erreur
 */
int put_list_bottom(struct _str_list_base **head, void *data);

/**
 * Prend la donnée en tête de liste et la retourne
 * @param[in] head L'adresse de la tête de la liste
 * @return La dernière donnée retirée de la liste
 */
void * rem_list_top(struct _str_list_base **head);

/**
 * Prend la donnée en tête de liste et la retourne
 * @param[in] head L'adresse de la tête de la liste
 * @return La dernière donnée retirée de la liste
 */
void * rem_list_bottom(struct _str_list_base **head);

/**
 * Renvoit la longueur de la liste
 * @param[in] head L'adresse de la tête de la liste
 * @return La longueur de la liste
 */
int get_list_len(struct _str_list_base *head);

/**
 * Libère la liste
 * @param[in] head L'adresse de la tête de la liste
 * @warning NE FREE PAS LES DONNÉES ! À LE FAIRE VOUS MÊME
 */
void free_str_list_base(struct _str_list_base **ptr);

/**
 * Libère la liste et ses données
 * @param[in] head L'adresse de la tête de la liste
 */
void free_str_list_base_data(struct _str_list_base **ptr);


/**
 * @brief un iterateur pour parcourir la liste avec un while ou un foreach
 * @code
 * iterator* it = initIterator(list);
 * do {
 *      obj = iteratorGet(&it)
 *	...
 * } while (iteratorNext(&it));
 * @endcode
 */
typedef struct {
	struct _str_list_base **list;
} iterator;

/**
 * Initialise un objet de type iterator
 * @param[in] head l'adresse du pointeur sur la tête de la liste
 * @return l'iterator
 * @remarks Pas d'allocation, ne pas free !
 */
iterator iterator_init(struct _str_list_base **head);

/**
 * Indique si on a atteint la fin de la liste
 * @param[in] it l'iterator instancié
 * @return 0 si l'iterator pointe sur le dernier chainon de la liste, sinon 1
 */
int iterator_has_next(iterator it);

/**
 * Incrémente l'iterator
 * @param it l'adresse de l'iterator instancié
 * @return 0 si le chainon est NULL (fin de liste), sinon 1
 */
int iterator_next(iterator *it);

/**
 * Renvoit la donnée du chainon actuel
 * @param[on] l'iterator instancié
 * @return la valeur contenu dans le chainon (peut être NULL)
 */
void * iterator_get(iterator it);

/**
 * Enlève le chainon actuel et renvoit sa donnée, l'iterator se place sur le
 * chainon suivant
 * @param it l'iterator instancié
 * @return la valeur contenu dans le chainon (peut être NULL)
 * @warning est équivalent à un iteratorNext
 */
void * iterator_remove(iterator *it);

#endif