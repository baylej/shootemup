#ifndef _GAME_H_
#define _GAME_H_

/* game.h : toutes les variables globales */

#include "output.h"
#include "list.h"
#include <gx.h>
#include "geometry.h"
#include "pmake.h"
#include "omake.h"

void draw(void); /* fonction de dessin appelée en continu */
void animate(void); /* fonction d'animation appelée à 60 FPS */

struct _bullet {
	GXpoint loc;
	GXvector speed;
};

struct _starship {
	GXpoint loc;
	GXpoint A;
	GXpoint B;
	GXpoint C;
	GXvector speed; /* Vecteur Vitesse (norme = 1)*/
	double health;
	double shield;
	int missile;
	uint score;
};

struct _asteroid {
	GXpoint loc;
	GXpoint *vertices;
	int v_length; /* nombre de sommets */
};

struct _turret {
	GXpoint center;
	double health;
	uchar color[3];
};

struct _kamikaze {
	GXpoint loc;
	GXpoint *vertices;
	int v_length;
	GXvector speed;
	uchar color[3];
	struct _bullet magic;
};

/* VARIABLES GLOBALES ICI */
#define W_TITLE  "Galaxy Snake Wars"
#define W_WIDTH  1000
#define W_HEIGHT 600

#define INITIAL_S_POS gx_Point(0.0,0.0)
#define INITIAL_S_HP  1.0
#define INITIAL_SPEED gx_Vector_XY(0.0, 1.0)

#define E 0.1 /*  E  */
#define LINE_THICK 2 /* epaisseur du trait */

#define SPEED_FACTOR 0.2 /* Coefficient appliqué au vecteur vitesse unitaire */
#define BSPEED_FACTOR 0.4 /* Même que précédent mais pour les balles */
#define KSPEED_FACTOR 0.04 /* Même que précédent mais pour les kamikazes */
#define SMOOTH_ROT_FACTOR 2.0 /* coeff inverse appliqué à la rotation */

#define ENEMY_FACTOR 10 /* Coeff de pop d'ennemis par niveau */
#define GAME_NEW  0 /* gamemode nouveau jeu */
#define GAME_BOSS 100 /* gamemode boss */

#define STARSHIP_B 0.8 /* base du triangle */
#define STARSHIP_H 1.0 /* hauteur du triangle */
#define MAX_VGONE 8 /* maximum de cotés des polygones ennemis et asteroides */
#define AS_RAY 1.2 /* Rayon des asteroides FIXME rendre randomized */
#define KA_RAY 0.6 /* Rayon des ennemis kamikazes */
#define TU_RAY 0.6 /* Rayon initial (100hp) des turrets */
#define BU_RAY 0.2 /* Rayon des balles */

#define S_DAMAGE 0.2 /* domages causés par le joueur */
#define E_DAMAGE 0.005 /* domages causés par les ennemis */
#define K_DAMAGE 0.01 /* domages causés par les kamikazes */

extern int gamemode; /* Level du jeu */
extern struct _starship starship; /* Starship contient les valeurs du vaisseau contrôlé par le joueur */
extern list asteroids; /* Astéroides la valeur est un struct _asteroid */
extern list turrets; /* Turrets */
extern list kamikazes; /* Kamikaze */
extern list bullets; /* Bullets */

#endif	/* _GAME_H_ */

