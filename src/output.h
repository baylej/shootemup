#ifndef _OUTPUT_H_
#define	_OUTPUT_H_

#include <stdio.h>

/* GROS blocs de definition de macros d'output divers ... */
#ifndef NO_OUTPUT
	#define ABORT(ret, err_msg) { fputs(err_msg, stderr); fputc('\n', stderr); return ret; }
	#define ABORTF(ret, err_msg, to_free) { fputs(err_msg, stderr); fputc('\n', stderr); free(to_free); return ret; }
	#define ABORTFF(ret, err_msg, to_free) { fputs(err_msg, stderr); fputc('\n', stderr); free(*(to_free)); free(to_free); return ret; }
	#define ABORTC(err_msg) { fputs(err_msg, stderr); fputc('\n', stderr); goto cleanup; }

	#ifdef ENABLE_DEBUG
		#include <stdarg.h>
		/* Macro affichant des informations de debogage */
		#define DEBUG(format, ...) fprintf(stderr, format, __VA_ARGS__)
	#else 
		#define DEBUG(format, ...)
	#endif

	#else
	#define ABORT(ret, err_msg) { return ret; }
	#define ABORTF(ret, err_msg, to_free) { free(to_free); return ret; }
	#define ABORTFF(ret, err_msg, to_free) { free(*(to_free)); free(to_free); return ret; }
	#define ABORTC(err_msg) goto cleanup
	#define DEBUG(format, ...)
#endif

#endif	/* _OUTPUT_H_ */

