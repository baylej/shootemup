#ifndef _PMAKE_H_
#define _PMAKE_H_
#include <gx.h>

/* Création de primitives */

/** 
 * crée les points A,B,C d'un triangle isocèle à partir de sa base, sa hauteur et son centre de gravité
 * CoG le centre de gravité du triangle
 * orient doit avoir une norme de 1 
 * base_length la base du triangle
 * height la hauteur du triangle
 * A (out) sommet A du triangle
 * B (out) sommet B du triangle
 * C (out) sommet C du triangle
 */
void make_isotris(GXpoint CoG, GXvector orient, double base_length, double height, GXpoint *A, GXpoint *B, GXpoint *C);

/**
 * Crée les points des sommets du polygone à partir du centre, du rayon et du nombre de faces.
 * C Centre du polygone
 * ray longueur du rayon
 * nbfaces nombre de faces du polygone
 * vertices (out) tableau de GXpoint (la taille de vertices doit être au moins égale à faces_count)
 */
void make_isogon(GXpoint C, double ray, int faces_count, GXpoint vertices[]);

#endif	/* _PMAKE_H_ */

